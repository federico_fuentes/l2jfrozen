package com.l2jfrozen.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ApplicationContextManager {
    private static String SPRING_CONFIG_LOCATION = null;
    private static FileSystemXmlApplicationContext ctx;

    protected ApplicationContextManager() {
    }

    public static void init(String configLocation) {
        SPRING_CONFIG_LOCATION = configLocation;
    }

    public static ApplicationContext getApplicationContext() {
        if (ctx == null) {
            if (SPRING_CONFIG_LOCATION == null) {
                throw new IllegalArgumentException("Please call static init(String) method passing location of the spring-config");
            }
            ctx = new FileSystemXmlApplicationContext(SPRING_CONFIG_LOCATION);
        }
        return ctx;
    }
}
