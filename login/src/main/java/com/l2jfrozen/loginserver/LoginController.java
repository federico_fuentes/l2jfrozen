package com.l2jfrozen.loginserver;

import com.l2jfrozen.configuration.LoginServerConfig;
import com.l2jfrozen.crypt.Base64;
import com.l2jfrozen.crypt.ScrambledKeyPair;
import com.l2jfrozen.database.GameServerInstanceManager;
import com.l2jfrozen.database.exception.WrongAccountCredentialException;
import com.l2jfrozen.database.manager.AccountManager;
import com.l2jfrozen.database.model.login.Account;
import com.l2jfrozen.loginserver.network.serverpackets.LoginFail.LoginFailReason;
import com.l2jfrozen.thread.GameServerInfo;
import com.l2jfrozen.thread.ServerStatus;
import com.l2jfrozen.thread.ServerThread;
import com.l2jfrozen.util.Util;
import com.l2jfrozen.util.random.Rnd;
import javolution.util.FastCollection.Record;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Map;

/**
 * This class ...
 *
 * @version $Revision: 1.7.4.3 $ $Date: 2005/03/27 15:30:09 $
 */
@Service
public class LoginController {
    protected class ConnectionChecker extends Thread {
        @Override
        public void run() {
            for (; ; ) {
                long now = System.currentTimeMillis();
                if (_stopNow) break;
                for (final L2LoginClient cl : _clients) {
                    try {

                        if (cl != null && now - cl.getConnectionStartTime() > LoginServerConfig.SESSION_TTL) {
                            cl.close(LoginFailReason.REASON_TEMP_PASS_EXPIRED);
                        } else {
                            _clients.remove(cl);
                        }
                    } catch (Exception e) {
                        LOGGER.error("", e);

                    }
                }
            }
        }
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class.getName());

    /**
     * Time before kicking the client if he didnt logged yet
     */
    private final static int LOGIN_TIMEOUT = 60 * 1000;

    /**
     * Clients that are on the LS but arent assocated with a account yet
     */
    protected static FastList<L2LoginClient> _clients = new FastList<>();

    /**
     * Authed Clients on LoginServer
     */
    protected static FastMap<String, L2LoginClient> _loginServerClients = new FastMap<String, L2LoginClient>().shared();

    private static Map<InetAddress, BanInfo> _bannedIps = new FastMap<InetAddress, BanInfo>().shared();

    private Map<InetAddress, FailedLoginAttempt> _hackProtection;
    protected static ScrambledKeyPair[] _keyPairs;

    protected static byte[][] _blowfishKeys;
    private static final int BLOWFISH_KEYS = 20;

    @PostConstruct
    public void load() throws GeneralSecurityException {
        Util.printSection("LoginController");

        _hackProtection = new FastMap<>();

        _keyPairs = new ScrambledKeyPair[10];
        KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
        RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4);
        keygen.initialize(spec);

        //generate the initial set of keys
        for (int i = 0; i < 10; i++) {
            _keyPairs[i] = new ScrambledKeyPair(keygen.generateKeyPair());
        }

        LOGGER.info("Cached 10 KeyPairs for RSA communication");

        testCipher((RSAPrivateKey) _keyPairs[0]._pair.getPrivate());

        generateBlowFishKeys();
        new ConnectionChecker().start();
    }

    private LoginController(){

    }

    /**
     * This is mostly to force the initialization of the Crypto Implementation, avoiding it being done on runtime when
     * its first needed.<BR>
     * In short it avoids the worst-case execution time on runtime by doing it on loading.
     *
     * @param key Any private RSA Key just for testing purposes.
     * @throws java.security.GeneralSecurityException if a underlying exception was thrown by the Cipher
     */
    private void testCipher(RSAPrivateKey key) throws GeneralSecurityException {
        Cipher.getInstance("RSA/ECB/nopadding").init(Cipher.DECRYPT_MODE, key);
    }

    protected static boolean _stopNow = false;

    public static void shutdown() {
        _stopNow = true;
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
            LOGGER.error("", e);

        }
    }

    private void generateBlowFishKeys() {
        _blowfishKeys = new byte[BLOWFISH_KEYS][16];

        for (int i = 0; i < BLOWFISH_KEYS; i++) {
            for (int j = 0; j < _blowfishKeys[i].length; j++) {
                _blowfishKeys[i][j] = (byte) (Rnd.nextInt(255) + 1);
            }
        }
        LOGGER.info("Stored " + _blowfishKeys.length + " keys for Blowfish communication");
    }

    /**
     * @return Returns a random key
     */
    public static byte[] getBlowfishKey() {
        return _blowfishKeys[(int) (Math.random() * BLOWFISH_KEYS)];
    }

    public static void addLoginClient(L2LoginClient client) {
        if (_clients.size() >= LoginServerConfig.MAX_LOGINSESSIONS) {
            for (L2LoginClient cl : _clients) {
                try {
                    cl.close(LoginFailReason.REASON_DUAL_BOX);
                } catch (Exception e) {
                    LOGGER.error("", e);

                }
            }
        }
        synchronized (_clients) {
            _clients.add(client);
        }
    }

    public static void removeLoginClient(L2LoginClient client) {
        if (_clients.contains(client)) {
            synchronized (_clients) {
                try {
                    _clients.remove(client);
                } catch (Exception e) {
                    LOGGER.error("", e);

                }
            }
        }
    }

    public static SessionKey assignSessionKeyToClient(String account, L2LoginClient client) {
        SessionKey key;

        key = new SessionKey(Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt());
        _loginServerClients.put(account, client);
        return key;
    }

    public static void removeAuthedLoginClient(String account) {
        try {
            _loginServerClients.remove(account);

        } catch (Exception e) {
            LOGGER.error("", e);

        }
    }

    public boolean isAccountInLoginServer(String account) {
        return _loginServerClients.containsKey(account);
    }

    public static L2LoginClient getAuthedClient(String account) {
        return _loginServerClients.get(account);
    }

    public static enum AuthLoginResult {
        INVALID_PASSWORD,
        ACCOUNT_BANNED,
        ALREADY_ON_LS,
        ALREADY_ON_GS,
        AUTH_SUCCESS
    }

    public static AuthLoginResult doAuthorized(String account, String password, L2LoginClient client) {
        AuthLoginResult ret;
        // check auth
        if (loginValid(account, password, client)) {
            // login was successful, verify presence on Gameservers
            ret = AuthLoginResult.ALREADY_ON_GS;

            if (!isAccountInAnyGameServer(account)) {
                // account isnt on any GS verify LS itself
                ret = AuthLoginResult.ALREADY_ON_LS;

                // dont allow 2 simultaneous login
                synchronized (_loginServerClients) {
                    if (!_loginServerClients.containsKey(account)) {
                        _loginServerClients.put(account, client);
                        ret = AuthLoginResult.AUTH_SUCCESS;

                        // remove him from the non-authed list
                        removeLoginClient(client);
                    }
                }
            }
            if (client.getAccessLevel() < 0) {
                return AuthLoginResult.ACCOUNT_BANNED;
            }
        } else {
            ret = AuthLoginResult.INVALID_PASSWORD;
        }
        return ret;
    }

    /**
     * Adds the address to the ban list of the login server, with the given duration.
     *
     * @param address    The Address to be banned.
     * @param expiration Timestamp in miliseconds when this ban expires
     * @throws java.net.UnknownHostException if the address is invalid.
     */
    public void addBanForAddress(String address, long expiration) throws UnknownHostException {

        InetAddress netAddress = InetAddress.getByName(address);
        _bannedIps.put(netAddress, new BanInfo(netAddress, expiration));
    }

    /**
     * Adds the address to the ban list of the login server, with the given duration.
     *
     * @param address  The Address to be banned.
     * @param duration is miliseconds
     */
    public static void addBanForAddress(InetAddress address, long duration) {
        _bannedIps.put(address, new BanInfo(address, System.currentTimeMillis() + duration));
    }

    public static boolean isBannedAddress(InetAddress address) {
        BanInfo bi = _bannedIps.get(address);
        if (bi != null) {
            if (bi.hasExpired()) {
                _bannedIps.remove(address);
                return false;
            }
            return true;
        }
        return false;
    }

    public Map<InetAddress, BanInfo> getBannedIps() {
        return _bannedIps;
    }

    /**
     * Remove the specified address from the ban list
     *
     * @param address The address to be removed from the ban list
     * @return true if the ban was removed, false if there was no ban for this ip
     */
    public boolean removeBanForAddress(InetAddress address) {
        return _bannedIps.remove(address) != null;
    }

    /**
     * Remove the specified address from the ban list
     *
     * @param address The address to be removed from the ban list
     * @return true if the ban was removed, false if there was no ban for this ip or the address was invalid.
     */
    public boolean removeBanForAddress(String address) {
        try {
            return this.removeBanForAddress(InetAddress.getByName(address));
        } catch (UnknownHostException e) {
            LOGGER.error("", e);

            return false;
        }
    }

    public static SessionKey getKeyForAccount(String account) {
        L2LoginClient client = _loginServerClients.get(account);

        if (client != null) {
            return client.getSessionKey();
        }

        return null;
    }

    public int getOnlinePlayerCount(int serverId) {
        GameServerInfo gsi = GameServerInstanceManager.getInstance().getServerInfo(serverId);

        if (gsi != null && gsi.isAuthed()) {
            return gsi.getCurrentPlayerCount();
        }
        return 0;
    }

    public static boolean isAccountInAnyGameServer(String account) {

        for (GameServerInfo gsi : GameServerInstanceManager.getInstance().getServers()) {
            ServerThread gst = gsi.getGameServerThread();

            if (gst != null && gst.hasAccountOnGameServer(account)) {
                return true;
            }
        }

        return false;
    }

    public static GameServerInfo getAccountOnGameServer(String account) {

        for (GameServerInfo gsi : GameServerInstanceManager.getInstance().getServers()) {
            ServerThread gst = gsi.getGameServerThread();

            if (gst != null && gst.hasAccountOnGameServer(account)) {
                return gsi;
            }
        }
        return null;
    }

    public int getTotalOnlinePlayerCount() {
        int total = 0;
        for (GameServerInfo gsi : GameServerInstanceManager.getInstance().getServers()) {
            if (gsi.isAuthed()) {
                total += gsi.getCurrentPlayerCount();
            }
        }
        return total;
    }

    public int getMaxAllowedOnlinePlayers(int id) {
        GameServerInfo gsi = GameServerInstanceManager.getInstance().getServerInfo(id);

        if (gsi != null) {
            return gsi.getMaxPlayers();
        }
        return 0;
    }

    /**
     * @param client
     * @param serverId
     * @return
     */
    public static boolean isLoginPossible(L2LoginClient client, int serverId) {
        GameServerInfo gsi = GameServerInstanceManager.getInstance().getServerInfo(serverId);
        int access = client.getAccessLevel();
        if (gsi != null && gsi.isAuthed()) {
            boolean loginOk = gsi.getCurrentPlayerCount() < gsi.getMaxPlayers() && gsi.getStatus() != ServerStatus.STATUS_GM_ONLY || access >= 100;
            if (loginOk && client.getLastServer() != serverId) {
                client.getAccount().setLastServer(serverId);
                AccountManager.getInstance().update(client.getAccount());
            }
            return loginOk;
        }
        return false;
    }

    public static void setAccountAccessLevel(String account, int banLevel) {

        /*TODO refactoring this code
        Connection con = null;
        try {
            con = L2DatabaseFactory.getInstance().getConnection(false);

            String stmt = "UPDATE accounts SET access_level=? WHERE login=?";
            PreparedStatement statement = con.prepareStatement(stmt);
            statement.setInt(1, banLevel);
            statement.setString(2, account);
            statement.executeUpdate();
            statement.close();
            statement = null;
        } catch (Exception e) {
            if (LoginServerConfig.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();

            LOGGER.warn("Could not set accessLevel: " + e);
        } finally {
            CloseUtil.close(con);
            con = null;
        }*/
        return;
    }

    public boolean isGM(String user) {
        boolean ok = false;
        /*TODO this code is obsolete
        Connection con = null;
        try {
            con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement statement = con.prepareStatement("SELECT access_level FROM accounts WHERE login=?");
            statement.setString(1, user);
            ResultSet rset = statement.executeQuery();

            if (rset.next()) {
                int accessLevel = rset.getInt(1);

                if (accessLevel >= 100) {
                    ok = true;
                }
            }

            rset.close();
            statement.close();
            statement = null;
            rset = null;
        } catch (Exception e) {
            if (LoginServerConfig.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();

            LOGGER.warn("could not check gm state:" + e);
            ok = false;
        } finally {
            CloseUtil.close(con);
            con = null;
        }*/
        return ok;
    }

    /**
     * <p>
     * This method returns one of the cached {@link ScrambledKeyPair ScrambledKeyPairs} for communication with Login
     * Clients.
     * </p>
     *
     * @return a scrambled keypair
     */
    public static ScrambledKeyPair getScrambledRSAKeyPair() {
        return _keyPairs[Rnd.nextInt(10)];
    }

    /**
     * login name is not case sensitive any more
     *
     * @param login
     * @param password
     * @param client
     * @return
     */
    public static synchronized boolean loginValid(String login, String password, L2LoginClient client) {

        InetAddress address = client.getConnection().getInetAddress();
        Account account = null;
        if (address == null) {
            return false;
        }

        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            byte[] raw = password.getBytes("UTF-8");
            byte[] hash = md.digest(raw);
            account = AccountManager.getInstance().get(login, Base64.encodeBytes(hash));

            // if account doesnt exists
            if (account == null) {
                if (LoginServerConfig.AUTO_CREATE_ACCOUNTS) {
                    if ((login != null) && (login.length()) >= 2 && (login.length() <= 14)) {
                        account = AccountManager.getInstance().create(login, Base64.encodeBytes(hash), 0, 0, address.getHostAddress());

                        if (account == null) {
                            return false;
                        }
                    }
                }
            }

        }catch (WrongAccountCredentialException e){
            LOGGER.warn("User '{}' input incorrect password",login);
            return false;
        } catch (Exception e) {
            LOGGER.error("",e);
            return false;
        }

        if (account == null) {
            return false;
        }
        AccountManager.getInstance().update(account);
        client.setAccount(account);
        client.setState(L2LoginClient.LoginClientState.AUTHED_LOGIN);
        return true;
    }

    public boolean loginBanned(String user) {
        boolean ok = false;

       /*TODO need modify
        Connection con = null;
        try {
            con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement statement = con.prepareStatement("SELECT access_level FROM accounts WHERE login=?");
            statement.setString(1, user);
            ResultSet rset = statement.executeQuery();

            if (rset.next()) {
                int accessLevel = rset.getInt(1);

                if (accessLevel < 0) {
                    ok = true;
                }
            }

            rset.close();
            statement.close();
            rset = null;
            statement = null;
        } catch (Exception e) {
            if (LoginServerConfig.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();

            // digest algo not found ??
            // out of bounds should not be possible
            LOGGER.warn("could not check ban state:" + e);
            ok = false;
        } finally {
            CloseUtil.close(con);
            con = null;
        }*/

        return true;
    }

    class FailedLoginAttempt {
        private int _count;
        private long _lastAttempTime;
        private String _lastPassword;

        public FailedLoginAttempt(InetAddress address, String lastPassword) {
            _count = 1;
            _lastAttempTime = System.currentTimeMillis();
            _lastPassword = lastPassword;
        }

        public void increaseCounter(String password) {
            if (!_lastPassword.equals(password)) {
                // check if theres a long time since last wrong try
                if (System.currentTimeMillis() - _lastAttempTime < 300 * 1000) {
                    _count++;
                } else {
                    // restart the status
                    _count = 1;

                }
                _lastPassword = password;
                _lastAttempTime = System.currentTimeMillis();
            } else
            //trying the same password is not brute force
            {
                _lastAttempTime = System.currentTimeMillis();
            }
        }

        public int getCount() {
            return _count;
        }
    }

    static class BanInfo {
        private InetAddress _ipAddress;
        // Expiration
        private long _expiration;

        public BanInfo(InetAddress ipAddress, long expiration) {
            _ipAddress = ipAddress;
            _expiration = expiration;
        }

        public InetAddress getAddress() {
            return _ipAddress;
        }

        public boolean hasExpired() {
            return System.currentTimeMillis() > _expiration && _expiration > 0;
        }
    }

    class PurgeThread extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (_clients) {
                    for (Record e = _clients.head(), end = _clients.tail(); (e = e.getNext()) != end; ) {
                        L2LoginClient client = _clients.valueOf(e);
                        if (client.getConnectionStartTime() + LOGIN_TIMEOUT >= System.currentTimeMillis()) {
                            client.close(LoginFailReason.REASON_ACCESS_FAILED);
                        }
                    }
                }

                synchronized (_loginServerClients) {
                    for (FastMap.Entry<String, L2LoginClient> e = _loginServerClients.head(), end = _loginServerClients.tail(); (e = e.getNext()) != end; ) {
                        L2LoginClient client = e.getValue();
                        if (client.getConnectionStartTime() + LOGIN_TIMEOUT >= System.currentTimeMillis()) {
                            client.close(LoginFailReason.REASON_ACCESS_FAILED);
                        }
                    }
                }

                try {
                    Thread.sleep(2 * LOGIN_TIMEOUT);
                } catch (InterruptedException e) {
                    LOGGER.error("", e);
                }
            }
        }
    }
}
