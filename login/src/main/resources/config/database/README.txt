# Database Pool Type
# Possible Values: c3p0 or BoneCP
# c3p0: more stable
# BoneCP: more performance
# More information about Database Pool Types at these links
# c3p0:
# http://www.mchange.com/projects/c3p0/
# BoneCP:
# http://jolbox.com/
