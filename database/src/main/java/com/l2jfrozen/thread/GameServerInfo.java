package com.l2jfrozen.thread;

/**
 * Created with IntelliJ IDEA.
 * User: vdidenko
 * Date: 03.12.13
 * Time: 0:15
 * To change this template use File | Settings | File Templates.
 */
public class GameServerInfo {
    // auth
    private int id;
    private byte[] hexId;
    private boolean isAuthed;

    // status
    private ServerThread serverThread;
    private ServerStatus status;

    // network
    private String _internalIp;
    private String _externalIp;
    private String _externalHost;
    private int _port;

    // config
    private boolean _isPvp = true;
    private boolean _isTestServer;
    private boolean _isShowingClock;
    private boolean _isShowingBrackets;
    private int _maxPlayers;

    public GameServerInfo(int id, byte[] hexId, ServerThread gst) {
        this.id = id;
        this.hexId = hexId;
        serverThread = gst;
        status = ServerStatus.STATUS_DOWN;
    }

    public GameServerInfo(int id, byte[] hexId) {
        this(id, hexId, null);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public byte[] getHexId() {
        return hexId;
    }

    public void setAuthed(boolean isAuthed) {
        this.isAuthed = isAuthed;
    }

    public boolean isAuthed() {
        return isAuthed;
    }

    public void setGameServerThread(ServerThread gst) {
        serverThread = gst;
    }

    public ServerThread getGameServerThread() {
        return serverThread;
    }

    public void setStatus(ServerStatus status) {
        this.status = status;
    }

    public ServerStatus getStatus() {
        return status;
    }

    public int getCurrentPlayerCount() {
        if (serverThread == null)
            return 0;

        return serverThread.getPlayerCount();
    }

    public void setInternalIp(String internalIp) {
        _internalIp = internalIp;
    }

    public String getInternalHost() {
        return _internalIp;
    }

    public void setExternalIp(String externalIp) {
        _externalIp = externalIp;
    }

    public String getExternalIp() {
        return _externalIp;
    }

    public void setExternalHost(String externalHost) {
        _externalHost = externalHost;
    }

    public String getExternalHost() {
        return _externalHost;
    }

    public int getPort() {
        return _port;
    }

    public void setPort(int port) {
        _port = port;
    }

    public void setMaxPlayers(int maxPlayers) {
        _maxPlayers = maxPlayers;
    }

    public int getMaxPlayers() {
        return _maxPlayers;
    }

    public boolean isPvp() {
        return _isPvp;
    }

    public void setTestServer(boolean val) {
        _isTestServer = val;
    }

    public boolean isTestServer() {
        return _isTestServer;
    }

    public void setShowingClock(boolean clock) {
        _isShowingClock = clock;
    }

    public boolean isShowingClock() {
        return _isShowingClock;
    }

    public void setShowingBrackets(boolean val) {
        _isShowingBrackets = val;
    }

    public boolean isShowingBrackets() {
        return _isShowingBrackets;
    }

    public void setDown() {
        setAuthed(false);
        setPort(0);
        setGameServerThread(null);
        setStatus(ServerStatus.STATUS_DOWN);
    }
}
