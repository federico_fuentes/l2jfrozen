package com.l2jfrozen.database.dal.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class ManagerImpl {
    protected static final Logger LOGGER = LoggerFactory.getLogger(ManagerImpl.class);
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Retrieves current session. If the method has been marked with @Transactional(readOnly=true) then the session
     * will be readonly.
     *
     * @return Session current locally (thread-local) bound session.
     */
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    protected <T> String buildConstraintOR(String key, List<T> values){
        String query="";
        if (values.size() == 1) {
            query=key+"='" + values.get(0) + "'";
        } else {
            query="(";
            int index = 1;
            for(int i=0;i<values.size();i++){
                T t=values.get(i);
                query=query+key+"='" + t + "'";
                if (index < values.size()) {
                    query=query+" OR ";
                } else {
                    query=query+")";
                }
                ++index;
            }
        }
        return query;
    }
}
