package com.l2jfrozen.database.dal.game.impl;

import com.l2jfrozen.database.dal.game.CharacterItemManagerDAO;
import com.l2jfrozen.database.dal.impl.ManagerImpl;
import com.l2jfrozen.database.model.game.character.CharacterItemEntity;
import com.l2jfrozen.database.model.game.ItemLocation;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * author vadim.didenko
 * 1/11/14.
 */
@Repository
public class CharacterItemManagerDAOImpl extends ManagerImpl implements CharacterItemManagerDAO {
    @Override
    @Transactional
    @Cacheable("gameCache")
    public CharacterItemEntity create(CharacterItemEntity item) {
        try {
            item.setId(null);
            getCurrentSession().persist(item);
            return item;
        } catch (Exception ex) {
            LOGGER.error("Got hibernate exception.", ex);
        }
        return null;
    }

    @Override
    @Transactional
    @CacheEvict(value = "gameCache")
    public void delete(CharacterItemEntity item) {
        try {
            getCurrentSession().delete(item);
            getCurrentSession().flush();
            getCurrentSession().clear();
        } catch (HibernateException e) {
            LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
        }
    }

    @Override
    @Transactional
    @CacheEvict(value = "gameCache")
    public void delete(long objectId) {
        try {
            CharacterItemEntity item = (CharacterItemEntity) getCurrentSession().createCriteria(CharacterItemEntity.class).add(Restrictions.eq("id", objectId)).uniqueResult();
            getCurrentSession().delete(item);
        } catch (HibernateException e) {
            LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
        }
    }

    @Override
    @Transactional
    @CacheEvict(value = "gameCache")
    public void delete(Long ownerId, List<Integer> itemIds) {
        try {
           getCurrentSession().createQuery("DELETE FROM CharacterItemEntity WHERE character.id=:ownerId AND"+ buildConstraintOR("itemId", itemIds)).setLong("ownerId", ownerId);
        } catch (HibernateException e) {
            LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
        }
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public CharacterItemEntity get(long objectId) {
        try {
            CharacterItemEntity item = (CharacterItemEntity)  getCurrentSession().createCriteria(CharacterItemEntity.class).add(Restrictions.eq("id",objectId)).uniqueResult();
            return item;
        } catch (Exception e) {
            LOGGER.error("Got hibernate exception.", e);
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public List<CharacterItemEntity> getByItemId(int itemId) {
        try {
            List<CharacterItemEntity> items = (List<CharacterItemEntity>) getCurrentSession().createCriteria(CharacterItemEntity.class).add(Restrictions.eq("itemId", itemId)).list();
            return items;
        } catch (Exception e) {
            LOGGER.error("Got hibernate exception.", e);
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public List<CharacterItemEntity> getOwnerItem(int ownerId) {
        try {
            return (List<CharacterItemEntity>) getCurrentSession().createCriteria(CharacterItemEntity.class).add(Restrictions.eq("ownerId", ownerId)).list();
        } catch (Exception e) {
            LOGGER.error("Got hibernate exception.", e);
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public List<CharacterItemEntity> getOwnerItem(int ownerId, List<ItemLocation> locations) {
        try {
            if (locations.isEmpty()) {
                LOGGER.error("location is empty");
                return new ArrayList<>();
            }
            List<CharacterItemEntity> items = getCurrentSession()
                    .createCriteria(CharacterItemEntity.class)
                    .add(Restrictions.and(Restrictions.eq("character.id", new Long(ownerId)), Restrictions.in("loc", locations))).list();
            return items;
        } catch (Exception e) {
            LOGGER.error("Got hibernate exception.", e);
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional
    public CharacterItemEntity update(CharacterItemEntity item) {
        try {
            if(item.getId()==null){
                return create(item);
            }
            getCurrentSession().update(item);
            return item;
        } catch (HibernateException e) {
            LOGGER.error("Got hibernate exception.", e);
        }
        return null;
    }

    @Override
    @Transactional
    public void updateItemLocation(int ownerId, ItemLocation itemLocation) {
        try {
            getCurrentSession().createQuery("UPDATE CharacterItemEntity SET loc=:loc WHERE character.id=:ownerId").setParameter("loc", itemLocation).setInteger("ownerId", ownerId);
            getCurrentSession().flush();
            getCurrentSession().clear();
        } catch (HibernateException e) {
            LOGGER.error("Got hibernate exception.", e);
        }
    }

    @Override
    @Transactional
    public void updateItemLocationType(int ownerId, ItemLocation itemLocation, ItemLocation newLocation) {
        try {
            getCurrentSession().createQuery("UPDATE CharacterItemEntity SET loc=:newLocation WHERE character.id=:ownerId AND loc=:itemLocation").setParameter("itemLocation", itemLocation).setParameter("newLocation", newLocation).setInteger("ownerId", ownerId);
            getCurrentSession().flush();
            getCurrentSession().clear();
        } catch (HibernateException e) {
            LOGGER.error("Got hibernate exception.", e);
        }
    }
}
