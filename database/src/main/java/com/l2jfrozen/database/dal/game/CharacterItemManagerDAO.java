package com.l2jfrozen.database.dal.game;

import com.l2jfrozen.database.model.game.character.CharacterItemEntity;
import com.l2jfrozen.database.model.game.ItemLocation;

import java.util.List;

/**
 * author vadim.didenko
 * 1/11/14.
 */
public interface CharacterItemManagerDAO {
    CharacterItemEntity create(CharacterItemEntity item);

    void delete(CharacterItemEntity item);

    void delete(long objectId);

    void delete(Long ownerId, List<Integer> itemId);

    CharacterItemEntity get(long objectId);

    List<CharacterItemEntity> getByItemId(int itemId);

    List<CharacterItemEntity> getOwnerItem(int ownerId);

    List<CharacterItemEntity> getOwnerItem(int ownerId, List<ItemLocation> itemLocation);

    CharacterItemEntity update(CharacterItemEntity item);

    void updateItemLocation(int ownerId, ItemLocation itemLocation);

    void updateItemLocationType(int ownerId, ItemLocation itemLocation, ItemLocation newLocation);
}
