package com.l2jfrozen.database.manager.game;

import com.l2jfrozen.context.SpringApplicationContext;
import com.l2jfrozen.database.dal.common.GenericManager;
import com.l2jfrozen.database.dal.game.CharacterItemManagerDAO;
import com.l2jfrozen.database.dal.game.CharacterManagerDAO;
import com.l2jfrozen.database.dal.game.CharacterSkillManagerDAO;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterItemEntity;
import com.l2jfrozen.database.model.game.ItemLocation;
import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;
import com.l2jfrozen.database.model.game.character.CharacterSkill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedOperation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * author vadim.didenko
 * 1/8/14.
 */

public class CharacterManager {
    protected static final Logger LOGGER = LoggerFactory.getLogger(CharacterManager.class);


    private static CharacterManager manager = null;
    private CharacterManagerDAO characterManagerDAO;
    private CharacterItemManagerDAO itemManagerDAO;
    private CharacterSkillManagerDAO skillManagerDAO;
    private GenericManager genericManager;

    public static CharacterManager getInstance() {
        if (manager == null) {
            manager = new CharacterManager();
        }
        return manager;
    }

    private CharacterManager() {
        characterManagerDAO = (CharacterManagerDAO) SpringApplicationContext.getBean(CharacterManagerDAO.class);
        itemManagerDAO = (CharacterItemManagerDAO) SpringApplicationContext.getBean(CharacterItemManagerDAO.class);
        genericManager = (GenericManager) SpringApplicationContext.getBean(GenericManager.class);
        skillManagerDAO = (CharacterSkillManagerDAO) SpringApplicationContext.getBean(CharacterSkillManagerDAO.class);
    }

    public CharacterFriendEntity addFriend(CharacterEntity characterInvite, CharacterFriendEntity requestFriend) {
        try {
            requestFriend.setCharacter(characterInvite);
            if (requestFriend.getId() == null) {
                requestFriend = characterManagerDAO.createFriend(requestFriend);
            }
            if (requestFriend.getId() != null) {
                return requestFriend;
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return null;
    }

    public CharacterSkill addSkill(CharacterSkill characterSkill) {
        return skillManagerDAO.addSkill(characterSkill);
    }

    public CharacterEntity create(CharacterEntity character) {
        character.setLastAccess(System.currentTimeMillis());
        return characterManagerDAO.create(character);
    }

    public CharacterItemEntity create(CharacterItemEntity item) {
        return itemManagerDAO.create(item);
    }

    public boolean deleteFriend(CharacterEntity character, CharacterEntity friendCharacter) {
        return characterManagerDAO.deleteFriend(character, friendCharacter);
    }

    /**
     * Delete character item
     *
     * @param item {@link com.l2jfrozen.database.model.game.character.CharacterItemEntity}
     */
    public void deleteItem(CharacterItemEntity item) {
        itemManagerDAO.delete(item);
    }

    /**
     * Delete item by unique item id
     *
     * @param objectId item id
     */
    public void deleteItem(long objectId) {
        itemManagerDAO.delete(objectId);
    }

    /**
     * Delete character item by item id
     *
     * @param ownerId character id
     * @param itemId  item id
     */
    public void deleteItem(Long ownerId, List<Integer> itemId) {
        itemManagerDAO.delete(ownerId, itemId);
    }

    public void deleteItemsById(CharacterEntity entity, List<Integer> itemIds) {
        Iterator<Map.Entry<Long, CharacterItemEntity>> iter = entity.getItems().entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Long, CharacterItemEntity> entry = iter.next();
            CharacterItemEntity itemEntity = entry.getValue();
            if (itemIds.contains(itemEntity.getItemId())) {
                iter.remove();
            }
        }
    }

    public void deleteSkills(Long charId, int classIndex) {
        skillManagerDAO.deleteSkills(charId, classIndex);
    }

    public List<CharacterEntity> get(String accountName) {
        return characterManagerDAO.get(accountName);
    }

    public CharacterEntity get(Long id) {
        return characterManagerDAO.get(id);
    }

    public CharacterEntity get(int ownerId) {
        return characterManagerDAO.get(((Number) ownerId).longValue());
    }

    public CharacterEntity getByCharacterName(String name) {
        return characterManagerDAO.getByName(name);
    }

    public List<CharacterEntity> getByClanId(int clanId) {
        return characterManagerDAO.getByClanId(clanId);
    }

    /**
     * Get items by item ID
     *
     * @param id item id
     * @return list of item
     */
    public List<CharacterItemEntity> getByItemId(int id) {
        return itemManagerDAO.getByItemId(id);
    }

    public List<CharacterEntity> getBySponsorId(int id, boolean isSponsor) {
        return characterManagerDAO.getSponsor(id);
    }

    public List<CharacterFriendEntity> getFriends(CharacterEntity character, FriendType type) {
        List<CharacterFriendEntity> friends = new ArrayList<>();
        if (type == null) {
            type = FriendType.ALL;
        }
        for (CharacterFriendEntity friend : character.getFriends().values()) {
            switch (type) {
                case ALL:
                    friends.add(friend);
                    break;
                case BLOCK:
                    if (friend.getNotBlocked() == 0) {
                        friends.add(friend);
                    }
                    break;
                case UNBLOCK:
                    if (friend.getNotBlocked() == 1) {
                        friends.add(friend);
                    }
            }
        }
        return friends;
    }

    /**
     * Get item by unique item id
     *
     * @param objectId - item id
     * @return item {@link com.l2jfrozen.database.model.game.character.CharacterItemEntity}
     */
    public CharacterItemEntity getItem(long objectId) {
        return itemManagerDAO.get(objectId);
    }

    /**
     * Ret all character items
     *
     * @param id owner id {@link com.l2jfrozen.database.model.game.character.CharacterEntity}
     * @return List of items
     */
    public List<CharacterItemEntity> getOwnerItem(int id) {
        return itemManagerDAO.getOwnerItem(id);
    }

    /**
     * Get all item for character by item location type
     *
     * @param ownerId   character id
     * @param locations location type list {@link com.l2jfrozen.database.model.game.character.CharacterEntity}
     * @return list of item {@link com.l2jfrozen.database.model.game.character.CharacterItemEntity}
     */
    public List<CharacterItemEntity> getOwnerItem(int ownerId, List<ItemLocation> locations) {
        return itemManagerDAO.getOwnerItem(ownerId, locations);
    }

    public CharacterEntity update(CharacterEntity character) {
        character.setLastAccess(System.currentTimeMillis());
        characterManagerDAO.update(character);
        return character;
    }

    /**
     * Update item data
     *
     * @param item - item {@link com.l2jfrozen.database.model.game.character.CharacterItemEntity}
     * @return - updated item {@link com.l2jfrozen.database.model.game.character.CharacterItemEntity}
     */
    public CharacterItemEntity update(CharacterItemEntity item) {
        itemManagerDAO.update(item);
        return item;
    }

    /**
     * Change character item location type {@link com.l2jfrozen.database.model.game.ItemLocation}
     *
     * @param ownerId      character object id
     * @param itemLocation location type {@link com.l2jfrozen.database.model.game.ItemLocation}
     */
    public void updateItemLocation(int ownerId, ItemLocation itemLocation) {
        itemManagerDAO.updateItemLocation(ownerId, itemLocation);
    }

    /**
     * Change item location type for character
     *
     * @param ownerId      character id
     * @param itemLocation location type {@link com.l2jfrozen.database.model.game.ItemLocation}
     * @param newLocation  new location type {@link com.l2jfrozen.database.model.game.ItemLocation}
     */
    public void updateItemLocationType(int ownerId, ItemLocation itemLocation, ItemLocation newLocation) {
        itemManagerDAO.updateItemLocationType(ownerId, itemLocation, newLocation);
    }

    public void deleteShortcuts(CharacterEntity character) {
        characterManagerDAO.deleteAllShortcuts(character);
    }

    public void deleteShortcutByClass(CharacterEntity character, int classIndex) {
        characterManagerDAO.deleteShortcutByClass(character,classIndex);
    }

    public static enum FriendType {
        BLOCK,
        UNBLOCK,
        ALL
    }

    public CharacterEntity getSavedSkills(CharacterEntity entity, int classIndex) {
        return skillManagerDAO.getSavedSkills(entity, classIndex);
    }

    public void clearCache(){
        characterManagerDAO.clearCache();
    }
}
