package com.l2jfrozen.database.manager;

import com.l2jfrozen.context.SpringApplicationContext;
import com.l2jfrozen.database.dal.common.GenericManager;
import com.l2jfrozen.database.exception.ConstraintException;
import com.l2jfrozen.database.exception.ManagerException;
import com.l2jfrozen.database.model.Identifiable;
import org.hibernate.criterion.Criterion;

import java.util.Collection;
import java.util.List;

/**
 * author vadim.didenko
 * 15.01.14.
 */
public class CommonManager {
    private static CommonManager commonManager;
    private GenericManager genericManager;

    private CommonManager() {
        genericManager = (GenericManager) SpringApplicationContext.getBean(GenericManager.class);
    }

    public static CommonManager getInstance() {
        if (commonManager == null) {
            commonManager = new CommonManager();
        }
        return commonManager;
    }

    public void delete(Object entity) throws ManagerException {
        genericManager.delete(entity);
    }

    public <T extends Identifiable> T update(T objectEdited) throws ConstraintException {
        return genericManager.update(objectEdited);
    }

    public void initialize(Object entity, Object proxy) {
        genericManager.initialize(entity, proxy);
    }

    public void refresh(Object entity) {
        genericManager.refresh(entity);
    }

    public <T extends Identifiable> List getObjects(Class<T> clazz, Collection<Long> ids) {
        return genericManager.getObjects(clazz, ids);
    }

    public int getEntityCount(Class entityClass, String searchPropertyName, String searchQuery) {
        return genericManager.getEntityCount(entityClass, searchPropertyName, searchQuery);
    }

    public <T extends Identifiable> List<T> getEntityList(Class<T> entityClass, String searchPropertyName, String searchQuery, String orderBy, Boolean isAscending, Integer first, Integer count) {
        return genericManager.getEntityList(entityClass, searchPropertyName, searchQuery, orderBy, isAscending, first, count);
    }

    public <T extends Identifiable> List<T> getEntityList(Class<T> entityClass, String orderBy) {
        return genericManager.getEntityList(entityClass, orderBy);
    }

    public <T extends Identifiable> List<T> getEntityList(Class<T> entityClass, List<String> joins, List<Criterion> restrictions, String searchPropertyName, String searchQuery, String orderBy, Boolean isAscending, Integer first, Integer count) {
        return genericManager.getEntityList(entityClass, joins, restrictions, searchPropertyName, searchQuery, orderBy, isAscending, first, count);
    }

    public <T extends Identifiable> T saveNew(T newObject) throws ConstraintException {
        return genericManager.saveNew(newObject);
    }

    public <T extends Identifiable> T getObject(Class<T> clazz, Long id) {
        return genericManager.getObject(clazz, id);
    }


}
