package com.l2jfrozen.database.manager;

import com.l2jfrozen.context.SpringApplicationContext;
import com.l2jfrozen.database.dal.login.AccountManagerDAO;
import com.l2jfrozen.database.exception.WrongAccountCredentialException;
import com.l2jfrozen.database.model.login.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * vdidenko
 * 07.12.13
 */
public class AccountManager {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AccountManager.class);
    private static AccountManager accountManager;
    private AccountManagerDAO accountManagerDao;

    public static AccountManager getInstance() {
        if (accountManager == null) {
            accountManager = new AccountManager();
        }
        return accountManager;
    }

    private AccountManager() {
        accountManagerDao = (AccountManagerDAO) SpringApplicationContext.getBean(AccountManagerDAO.class);
    }

    public Account create(String login, String password, int lastServer, int accessLevel, String lastIp) {
        Account account = new Account();
        account.setLogin(login);
        account.setPassword(password);
        account.setAccessLevel(accessLevel);
        account.setLastServer(lastServer);
        account.setLastIp(lastIp);
        return accountManagerDao.add(account);
    }

    public Account get(String login, String password) throws WrongAccountCredentialException {
        try {
            Account account = accountManagerDao.get(login);
            if (account != null && !account.getPassword().equals(password)) {
                throw new WrongAccountCredentialException("Wrong login or password");
            }
            return account;
        } catch (SQLException e) {
            LOGGER.error("Account load fault", e);
        }
        return null;
    }

    public Account update(Account account) {
        return accountManagerDao.update(account);
    }
}
