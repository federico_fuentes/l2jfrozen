package com.l2jfrozen.database.model.game.character;

import com.l2jfrozen.database.model.AbstractIdentifiable;

import javax.persistence.*;

/**
 * author vadim.didenko
 * 10.02.14
 */
@Entity
@Table(name = "character_admin_command_access_right")
public class CharacterAdminCommandAccessRight extends AbstractIdentifiable {
    private String adminCommand;
    private CharacterAccessLevel accessLevel;

    @Column(name = "admin_command",length = 64)
    public String getAdminCommand() {
        return adminCommand;
    }

    public void setAdminCommand(String adminCommand) {
        this.adminCommand = adminCommand;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "access_level")
    public CharacterAccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(CharacterAccessLevel accessLevels) {
        this.accessLevel = accessLevels;
    }
}
