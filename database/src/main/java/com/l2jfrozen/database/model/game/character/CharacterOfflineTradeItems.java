package com.l2jfrozen.database.model.game.character;

import com.l2jfrozen.database.model.AbstractIdentifiable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * author vadim.didenko
 * 1/12/14.
 */
@Entity
@javax.persistence.Table(name = "character_offline_trade_items")
public class CharacterOfflineTradeItems implements Serializable{

    @Id
    private int charId;

    @Id
    private int item;

    private int count;

    private int price;

    @Id
    private int enchant;


    @Basic
    @javax.persistence.Column(name = "charId")
    public int getCharId() {
        return charId;
    }

    public void setCharId(int charId) {
        this.charId = charId;
    }

    @Basic
    @javax.persistence.Column(name = "count")
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Basic
    @javax.persistence.Column(name = "enchant")
    public int getEnchant() {
        return enchant;
    }

    public void setEnchant(int enchant) {
        this.enchant = enchant;
    }

    @Basic
    @javax.persistence.Column(name = "item")
    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    @Basic
    @javax.persistence.Column(name = "price")
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
