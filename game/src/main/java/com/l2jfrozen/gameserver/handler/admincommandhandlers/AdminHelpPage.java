/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.admincommandhandlers;

import com.l2jfrozen.gameserver.cache.HtmCache;
import com.l2jfrozen.gameserver.handler.IAdminCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class handles following admin commands: - help path = shows /data/html/admin/path file to char, should not be
 * used by GM's directly
 *
 * @version $Revision: 1.2.4.3 $ $Date: 2005/04/11 10:06:02 $
 */
public class AdminHelpPage implements IAdminCommandHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminHelpPage.class.getName());
    private static final String[] ADMIN_COMMANDS =
            {
                    "admin_help"
            };

    //FIXME: implement method to send html to player in L2PcInstance directly
    //PUBLIC & STATIC so other classes from package can include it directly
    public static void showHelpPage(L2PcInstance targetChar, String filename) {
        String content = HtmCache.getInstance().getHtmForce("data/html/admin/" + filename);
        NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
        adminReply.setHtml(content);
        targetChar.sendPacket(adminReply);
        content = null;
        adminReply = null;
    }

    public static void showSubMenuPage(L2PcInstance targetChar, String filename) {
        String content = HtmCache.getInstance().getHtmForce("data/html/admin/" + filename);
        NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
        adminReply.setHtml(content);
        targetChar.sendPacket(adminReply);
        content = null;
        adminReply = null;
    }

    @Override
    public boolean useAdminCommand(String command, L2PcInstance activeChar) {

        if (command.startsWith("admin_help")) {
            try {
                String val = command.substring(11);
                showHelpPage(activeChar, val);
                val = null;
            } catch (StringIndexOutOfBoundsException e) {
                LOGGER.warn("Incorrect command");
            }
        }

        return true;
    }

    @Override
    public String[] getAdminCommandList() {
        return ADMIN_COMMANDS;
    }
}
