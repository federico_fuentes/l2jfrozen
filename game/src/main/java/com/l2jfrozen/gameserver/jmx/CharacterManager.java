package com.l2jfrozen.gameserver.jmx;

import com.l2jfrozen.database.dal.game.CharacterManagerDAO;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.gameserver.datatables.sql.AccessManager;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import org.hibernate.Cache;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Чистка кеша по расписанию
 * author vadim.didenko
 * 29.01.14
 */
@Service
@ManagedResource(objectName = "com.l2jfrozen:application=Game, name=CacheClearScheduler")
public class CharacterManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(CharacterManager.class);
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private CharacterManagerDAO characterManager;

    @ManagedOperation(description = "Clean cache")
    @Scheduled(cron = "${rates.refresh.cron}")
    public void run() {
        LOGGER.info("Start clear hibernate cache");
        Cache cache = sessionFactory.getCache();
        cache.evictCollectionRegions();
        cache.evictDefaultQueryRegion();
        cache.evictEntityRegions();
        cache.evictQueryRegions();
        cache.evictNaturalIdRegions();
        characterManager.clearCache();
    }
    @ManagedOperation(description = "Change character access level")
    @ManagedOperationParameters({
            @ManagedOperationParameter(name = "name", description = "Character name"),
            @ManagedOperationParameter( name="level",description="Character access level")
    })
    public void changeAccessLevel(String name,int level){
        CharacterEntity entity= characterManager.getByName(name);
        if(entity==null){
            LOGGER.warn("character {} not found",name);
            return;
        }
        entity.setAccessLevel(AccessManager.getInstance().getAccessLevel(level).getAccessType());
        characterManager.update(entity);
        LOGGER.info("character {} change access level success", name);
        L2PcInstance pcInstance= L2World.getInstance().getPlayer(name);
        if(pcInstance==null){
           return;
        }
        pcInstance.setAccessLevel(level);
    }
}
