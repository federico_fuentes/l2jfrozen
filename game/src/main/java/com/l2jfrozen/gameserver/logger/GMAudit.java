package com.l2jfrozen.gameserver.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * vadim.didenko
 * 2/27/14.
 */
public class GMAudit {
    private static final Logger LOGGER = LoggerFactory.getLogger(GMAudit.class);
    private static GMAudit instance;
    public static GMAudit getInstance() {
        return instance==null?instance=new GMAudit():instance;
    }

    public void chat(String message){
        LOGGER.info(message);
    }
}
