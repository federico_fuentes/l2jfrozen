/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.datatables.sql;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.database.manager.CommonManager;
import com.l2jfrozen.database.model.game.AccessLevelType;
import com.l2jfrozen.database.model.game.character.CharacterAccessLevel;
import com.l2jfrozen.database.model.game.character.CharacterAdminCommandAccessRight;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author FBIagent<br>
 */
public class AdminCommandAccessRights {
    /**
     * The logger<br>
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AdminCommandAccessRights.class.getName());

    /**
     * The one and only instance of this class, retriveable by getInstance()<br>
     */
    private static AdminCommandAccessRights _instance = null;

    /**
     * The access rights<br>
     */
    private final Map<String, AccessLevelType> _adminCommandAccessRights = new FastMap<>();

    public static void reload() {
        _instance = null;
        getInstance();
    }

    /**
     * Returns the one and only instance of this class<br>
     * <br>
     *
     * @return AdminCommandAccessRights: the one and only instance of this class<br>
     */

    public static AdminCommandAccessRights getInstance() {
        return _instance == null ? (_instance = new AdminCommandAccessRights()) : _instance;
    }

    /**
     * Loads admin command access rights from database<br>
     */
    private AdminCommandAccessRights() {
        try {
            for (CharacterAdminCommandAccessRight accessRight : CommonManager.getInstance().getEntityList(CharacterAdminCommandAccessRight.class, "adminCommand")) {
                _adminCommandAccessRights.put(accessRight.getAdminCommand(), accessRight.getAccessLevel().getAccessType());
            }
        } catch (Exception e) {
            LOGGER.error("Admin Access Rights: Error loading from database", e);
        }

        LOGGER.debug("Admin Access Rights: Loaded {} Access Rigths from database." + " " + _adminCommandAccessRights.size());
    }

    public AccessLevelType accessRightForCommand(String command) {
        AccessLevelType out = AccessLevelType.BANNED;

        if (_adminCommandAccessRights.containsKey(command)) {
            out = _adminCommandAccessRights.get(command);
        }

        return out;
    }

    public boolean hasAccess(String adminCommand, CharacterAccessLevel characterAccessLevel) {
        if (characterAccessLevel.getAccessType() == AccessLevelType.BANNED && characterAccessLevel.getAccessType() == AccessLevelType.DEFAULT)
            return false;

        if (!characterAccessLevel.isGm())
            return false;

        if (characterAccessLevel.getAccessType().ordinal() == GameServerConfig.MASTERACCESS_LEVEL)
            return true;

        //L2EMU_ADD - Visor123  need parse command before check
        String command = adminCommand;
        if (adminCommand.contains(" ")) {
            command = adminCommand.substring(0, adminCommand.indexOf(" "));
        }
        //L2EMU_ADD

        AccessLevelType acar = AccessLevelType.DEFAULT;
        if (_adminCommandAccessRights.get(command) != null) {
            acar = _adminCommandAccessRights.get(command);
        }

        if (acar == AccessLevelType.DEFAULT) {
            LOGGER.warn("Admin Access Rights: No rights defined for admin command {}." + " " + command);
            return false;
        } else return acar.ordinal() >= characterAccessLevel.getAccessType().ordinal();
    }
}
