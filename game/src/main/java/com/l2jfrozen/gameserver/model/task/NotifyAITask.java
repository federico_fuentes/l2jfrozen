package com.l2jfrozen.gameserver.model.task;

import com.l2jfrozen.gameserver.ai.CtrlEvent;
import com.l2jfrozen.gameserver.ai.L2CharacterAI;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.actor.stat.CharStat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: vadimDidenko
 * Date: 17.12.13
 * Time: 21:28
 */
public class NotifyAITask<C extends CharStat,A extends L2CharacterAI,T extends L2Character<C,A>> implements Runnable {
    protected static final Logger LOGGER = LoggerFactory.getLogger(NotifyAITask.class.getName());
    /**
     * The event.
     */
    protected final CtrlEvent event;
    private T actor;

    public NotifyAITask(CtrlEvent evt,T actor) {
        event = evt;
        this.actor=actor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        try {
            actor.getAI().notifyEvent(event, null);
        } catch (Throwable t) {
            LOGGER.info("", t);
        }
    }
}
